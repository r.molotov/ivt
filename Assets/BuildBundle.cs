﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class BuildBundle : Editor
{
    [MenuItem("Assets/ PackBundle")]
    static void PackBundle()
    {
        BuildPipeline.BuildAssetBundles(
            @"D:\temp\bundle",
            BuildAssetBundleOptions.ChunkBasedCompression,
            BuildTarget.StandaloneWindows64
            );
    }
}
