﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] float lifeTime;
    float timer = 0;
    [SerializeField][Range(1, 100)] int damage = 1;
    void OnTriggerEnter2D(Collider2D col)
    {
        HP hp;
        if (hp = col.GetComponent<HP>())
        {
            hp?.Change(this, new AttackEventArgs("", damage));
            Destroy(gameObject);
        }
    }

    void Update()
    {
        if (timer >= lifeTime)
            Destroy(gameObject);
        timer += Time.deltaTime;
        transform.Translate(-transform.right * Time.deltaTime * 0.1f);
    }
}
