﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public Text scoreText;
    [HideInInspector] public Hero hero;
    public Text hint;
    #region Singleton
    private static GameManager _instance;
    public static GameManager GetInstance() { return _instance;  }
    void Awake()
    {
        if (GameManager.GetInstance() == null)
            _instance = this;
    }
    #endregion Singleton

    // Update is called once per frame
    void Update()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        hero?.movement.Move(h, v);

        if (Input.GetKeyDown(KeyCode.Space))
            hero?.movement.Jump();
    }
    public void LoadLevel(int lvl)
    {
        SceneManager.LoadScene(lvl);
    }
    public void ExitGame()
    {
        Application.Quit();
    }
}
