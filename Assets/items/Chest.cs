﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : Interactable
{
    // Start is called before the first frame update
    public override void Use()
    {
        base.Use();
        foreach (Transform t in transform)
        {
            t.gameObject.SetActive(true);
        }
        print("used");
        Destroy(GetComponent<Collider2D>());
    }
}
